ARG BASE_VERSION=8.6_p1-r3-ls74

FROM linuxserver/openssh-server:$BASE_VERSION

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

RUN apk add --no-cache --upgrade rsync \
  && rm -rf /var/cache/apk/*

LABEL "maintainer"="quentin9696" \
      "org.label-schema.name"="rsync-ssh" \
      "org.label-schema.base-image.name"="docker.io/linuxserver/openssh-server" \
      "org.label-schema.base-image.version"="$BASE_VERSION" \
      "org.label-schema.description"="Rsync with SSH server in a container" \
      "org.label-schema.url"="$VCS_REF" \
      "org.label-schema.vcs-url"="$VCS_REF" \
      "org.label-schema.vendor"="quentin9696" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"="See README.md"

