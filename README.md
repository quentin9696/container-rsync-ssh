# container-rsync-ssh

Rsync with SSH based on linuxserver.io openssh-server image

## Usage

See [linuxserver.io/docker-openssh-server](https://github.com/linuxserver/docker-openssh-server)
